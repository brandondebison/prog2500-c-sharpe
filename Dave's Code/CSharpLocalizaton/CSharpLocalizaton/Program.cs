﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace CSharpLocalizaton
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // This will retrieve a custom applicaiton setting created in the app.config file.
            // it is fed into method that will set the Current Culture info to the language specified.
            // This Must be done before the form is created or the form needs to be recreated after doing so 
            // to pick up the correct localization strings.
            string lang = ConfigurationManager.AppSettings["language"];
            if (lang != "" && lang != null)
            {
               ChangeLanguage(lang);
            }


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormName());
        }

        static void ChangeLanguage(string Language)
        {
            CultureInfo Culture = new CultureInfo(Language);
            Thread.CurrentThread.CurrentCulture = Culture;
            Thread.CurrentThread.CurrentUICulture = Culture;
        }
    }
}
