﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace CSharpLocalizaton
{
    public partial class FormName : Form
    {
        public FormName()
        {
            InitializeComponent();
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            // Show the message box using localized strings
            // Localization is the name of the class that was generated based on the file name 
            //given to the resource file. The File can be named anything you like.
            // Notice how the other languages add a . and the language  (localization.fr.resx) 
            if (txtName.Text.Trim() != "")
            { 
                lblName.Text = txtName.Text;
                MessageBox.Show(Localization.MsgBoxNameSetText, Localization.MsgBoxNameSetTitle);
            }
            else
            {
                MessageBox.Show(Localization.MsgBoxNoNameText, Localization.MsgBoxNameSetTitle);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblName.Text = "";
            setToolTips();
        }

        private void setToolTips()
        {
            //Tool tips are also set using localized strings.
            toolTipName.SetToolTip(btnSet,Localization.ToolTipbtnSet );
            toolTipName.SetToolTip(txtName, Localization.ToolTiptxtName);
        }
        
    }
}
