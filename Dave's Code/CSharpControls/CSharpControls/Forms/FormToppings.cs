﻿using System;
using System.IO;
using System.Windows.Forms;

namespace CSharpControls.Forms
{
    public partial class FormToppings : Form
    {
        public string file = "";
        private bool changed = false;
        public FormToppings()
        {
            InitializeComponent();
        }

        private void timerSave_Tick(object sender, EventArgs e)
        {
            if (cbAutoSave.Checked && changed && file != "")
            {
                saveToppings();
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            loadToppings();
            showEditControls();
        }

        private void loadToppings()
        {
            openFile.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            try
            {
                string line = "";
                openFile.ShowDialog();
                if (openFile.FileName != "")
                {
                    StreamReader rdr = new StreamReader(openFile.FileName);
                    file = lblFile.Text = openFile.FileName;
                    line = rdr.ReadLine();
                    while (line != null)
                    {
                        progressToppings.Increment(1);
                        lbxToppings.Items.Add(line);
                        line = rdr.ReadLine();
                    }
                    rdr.Close();
                    btnSave.Visible = true;
                    tsToppings.Text = "File Loaded.";
                    progressToppings.Value = 0;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void saveToppings()
        {
            try
            {
                progressToppings.Maximum = lbxToppings.Items.Count;
                StreamWriter wtr = new StreamWriter(file);
                statusToppings.Text = "Saving....";
                foreach (string topping in lbxToppings.Items)
                {
                    progressToppings.Increment(1);
                    wtr.WriteLine(topping);
                }
                wtr.Close();
                statusToppings.Text = "Saved.";
                changed = false;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtAdd.Text != "")
            {
                lbxToppings.Items.Add(txtAdd.Text);
                changed = true;
            } 
            else
            {
                txtAdd.Focus();
                MessageBox.Show("Enter a Topping");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Save Topppings?","Save?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                saveToppings();
            }
        }

        private void showEditControls()
        {
            btnAdd.Visible = true;
            txtAdd.Visible = true;
            if (lbxToppings.Items.Count > 0)
            {
                btnRemove.Visible = true;
            }

        }

        private void RemoveItem()
        {
            if (lbxToppings.SelectedIndex != -1)
            {
                lbxToppings.Items.RemoveAt(lbxToppings.SelectedIndex);
                changed = true;
                if (lbxToppings.Items.Count == 0)
                {
                    btnRemove.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("Select an item to remove.");
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            RemoveItem();
        }

        private void cbAutoSave_CheckedChanged(object sender, EventArgs e)
        {
            timerSave.Enabled = cbAutoSave.Checked;
        }

        private void FormToppings_Load(object sender, EventArgs e)
        {
            lblFile.Text = "";

        }

        private void cmiRemove_Click(object sender, EventArgs e)
        {
            RemoveItem();
        }

        private void cmCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lbxToppings.Text, TextDataFormat.Text);
        }

        private void lbxToppings_MouseDown(object sender, MouseEventArgs e)
        {
            lbxToppings.SelectedIndex = lbxToppings.IndexFromPoint(e.X, e.Y);
        }
    }
}
