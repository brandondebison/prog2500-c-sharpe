﻿namespace CSharpControls.Forms
{
    partial class FormToppings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerSave = new System.Windows.Forms.Timer(this.components);
            this.statusToppings = new System.Windows.Forms.StatusStrip();
            this.tsToppings = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressToppings = new System.Windows.Forms.ToolStripProgressBar();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbxToppings = new System.Windows.Forms.ListBox();
            this.contetMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.cmiRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblFile = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtAdd = new System.Windows.Forms.TextBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.cbAutoSave = new System.Windows.Forms.CheckBox();
            this.statusToppings.SuspendLayout();
            this.contetMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerSave
            // 
            this.timerSave.Interval = 90000;
            this.timerSave.Tick += new System.EventHandler(this.timerSave_Tick);
            // 
            // statusToppings
            // 
            this.statusToppings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsToppings,
            this.progressToppings});
            this.statusToppings.Location = new System.Drawing.Point(0, 357);
            this.statusToppings.Name = "statusToppings";
            this.statusToppings.Size = new System.Drawing.Size(367, 22);
            this.statusToppings.TabIndex = 0;
            this.statusToppings.Text = "statusStrip1";
            // 
            // tsToppings
            // 
            this.tsToppings.AutoSize = false;
            this.tsToppings.Name = "tsToppings";
            this.tsToppings.Size = new System.Drawing.Size(100, 17);
            // 
            // progressToppings
            // 
            this.progressToppings.Maximum = 8;
            this.progressToppings.Name = "progressToppings";
            this.progressToppings.Size = new System.Drawing.Size(100, 16);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(68, 52);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Load File";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Browse For Toppings";
            // 
            // lbxToppings
            // 
            this.lbxToppings.ContextMenuStrip = this.contetMenu;
            this.lbxToppings.FormattingEnabled = true;
            this.lbxToppings.Location = new System.Drawing.Point(68, 81);
            this.lbxToppings.Name = "lbxToppings";
            this.lbxToppings.Size = new System.Drawing.Size(165, 173);
            this.lbxToppings.TabIndex = 3;
            this.lbxToppings.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbxToppings_MouseDown);
            // 
            // contetMenu
            // 
            this.contetMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmCopy,
            this.cmiRemove});
            this.contetMenu.Name = "contetMenu";
            this.contetMenu.Size = new System.Drawing.Size(118, 48);
            // 
            // cmCopy
            // 
            this.cmCopy.Name = "cmCopy";
            this.cmCopy.Size = new System.Drawing.Size(152, 22);
            this.cmCopy.Text = "Copy";
            this.cmCopy.Click += new System.EventHandler(this.cmCopy_Click);
            // 
            // cmiRemove
            // 
            this.cmiRemove.Name = "cmiRemove";
            this.cmiRemove.Size = new System.Drawing.Size(152, 22);
            this.cmiRemove.Text = "Remove";
            this.cmiRemove.Click += new System.EventHandler(this.cmiRemove_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(68, 260);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Location = new System.Drawing.Point(65, 286);
            this.lblFile.MaximumSize = new System.Drawing.Size(300, 0);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(57, 13);
            this.lblFile.TabIndex = 5;
            this.lblFile.Text = "[FileName]";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(239, 110);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtAdd
            // 
            this.txtAdd.Location = new System.Drawing.Point(239, 139);
            this.txtAdd.Name = "txtAdd";
            this.txtAdd.Size = new System.Drawing.Size(100, 20);
            this.txtAdd.TabIndex = 7;
            this.txtAdd.Visible = false;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(239, 81);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 8;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Visible = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // cbAutoSave
            // 
            this.cbAutoSave.AutoSize = true;
            this.cbAutoSave.Location = new System.Drawing.Point(157, 264);
            this.cbAutoSave.Name = "cbAutoSave";
            this.cbAutoSave.Size = new System.Drawing.Size(76, 17);
            this.cbAutoSave.TabIndex = 9;
            this.cbAutoSave.Text = "Auto Save";
            this.cbAutoSave.UseVisualStyleBackColor = true;
            this.cbAutoSave.CheckedChanged += new System.EventHandler(this.cbAutoSave_CheckedChanged);
            // 
            // FormToppings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 379);
            this.Controls.Add(this.cbAutoSave);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.txtAdd);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblFile);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lbxToppings);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.statusToppings);
            this.Name = "FormToppings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Toppings";
            this.Load += new System.EventHandler(this.FormToppings_Load);
            this.statusToppings.ResumeLayout(false);
            this.statusToppings.PerformLayout();
            this.contetMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerSave;
        private System.Windows.Forms.StatusStrip statusToppings;
        private System.Windows.Forms.ToolStripStatusLabel tsToppings;
        private System.Windows.Forms.ToolStripProgressBar progressToppings;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbxToppings;
        private System.Windows.Forms.ContextMenuStrip contetMenu;
        private System.Windows.Forms.ToolStripMenuItem cmCopy;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.CheckBox cbAutoSave;
        private System.Windows.Forms.ToolStripMenuItem cmiRemove;
    }
}