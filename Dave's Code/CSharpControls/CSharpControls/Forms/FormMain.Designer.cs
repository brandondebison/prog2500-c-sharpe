﻿namespace CSharpControls.Forms
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tpPizza = new System.Windows.Forms.TabPage();
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.rbNo = new System.Windows.Forms.RadioButton();
            this.rbYes = new System.Windows.Forms.RadioButton();
            this.cbLikePizza = new System.Windows.Forms.CheckBox();
            this.dtpBirthday = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tpMovie = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxDirector = new System.Windows.Forms.ComboBox();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miSave = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.miTopping = new System.Windows.Forms.ToolStripMenuItem();
            this.ssMain = new System.Windows.Forms.StatusStrip();
            this.statLblMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.progBarMain = new System.Windows.Forms.ToolStripProgressBar();
            this.clbMovies = new System.Windows.Forms.CheckedListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.tabMain.SuspendLayout();
            this.tpPizza.SuspendLayout();
            this.tpMovie.SuspendLayout();
            this.msMain.SuspendLayout();
            this.ssMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tpPizza);
            this.tabMain.Controls.Add(this.tpMovie);
            this.tabMain.Location = new System.Drawing.Point(31, 38);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(427, 347);
            this.tabMain.TabIndex = 0;
            // 
            // tpPizza
            // 
            this.tpPizza.Controls.Add(this.btnSave);
            this.tpPizza.Controls.Add(this.label3);
            this.tpPizza.Controls.Add(this.rbNo);
            this.tpPizza.Controls.Add(this.rbYes);
            this.tpPizza.Controls.Add(this.cbLikePizza);
            this.tpPizza.Controls.Add(this.dtpBirthday);
            this.tpPizza.Controls.Add(this.label2);
            this.tpPizza.Controls.Add(this.txtName);
            this.tpPizza.Controls.Add(this.label1);
            this.tpPizza.Location = new System.Drawing.Point(4, 22);
            this.tpPizza.Name = "tpPizza";
            this.tpPizza.Padding = new System.Windows.Forms.Padding(3);
            this.tpPizza.Size = new System.Drawing.Size(419, 321);
            this.tpPizza.TabIndex = 0;
            this.tpPizza.Text = "Pizza";
            this.tpPizza.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(27, 139);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "With Anchovies";
            // 
            // rbNo
            // 
            this.rbNo.AutoSize = true;
            this.rbNo.Enabled = false;
            this.rbNo.Location = new System.Drawing.Point(294, 86);
            this.rbNo.Name = "rbNo";
            this.rbNo.Size = new System.Drawing.Size(39, 17);
            this.rbNo.TabIndex = 6;
            this.rbNo.TabStop = true;
            this.rbNo.Text = "No";
            this.rbNo.UseVisualStyleBackColor = true;
            // 
            // rbYes
            // 
            this.rbYes.AutoSize = true;
            this.rbYes.Enabled = false;
            this.rbYes.Location = new System.Drawing.Point(245, 85);
            this.rbYes.Name = "rbYes";
            this.rbYes.Size = new System.Drawing.Size(43, 17);
            this.rbYes.TabIndex = 5;
            this.rbYes.TabStop = true;
            this.rbYes.Text = "Yes";
            this.rbYes.UseVisualStyleBackColor = true;
            // 
            // cbLikePizza
            // 
            this.cbLikePizza.AutoSize = true;
            this.cbLikePizza.Location = new System.Drawing.Point(27, 86);
            this.cbLikePizza.Name = "cbLikePizza";
            this.cbLikePizza.Size = new System.Drawing.Size(79, 17);
            this.cbLikePizza.TabIndex = 4;
            this.cbLikePizza.Text = "Likes Pizza";
            this.cbLikePizza.UseVisualStyleBackColor = true;
            this.cbLikePizza.CheckedChanged += new System.EventHandler(this.cbLikePizza_CheckedChanged);
            // 
            // dtpBirthday
            // 
            this.dtpBirthday.Location = new System.Drawing.Point(151, 40);
            this.dtpBirthday.Name = "dtpBirthday";
            this.dtpBirthday.Size = new System.Drawing.Size(200, 20);
            this.dtpBirthday.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(148, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "BirthDate";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(27, 40);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // tpMovie
            // 
            this.tpMovie.Controls.Add(this.panel1);
            this.tpMovie.Location = new System.Drawing.Point(4, 22);
            this.tpMovie.Name = "tpMovie";
            this.tpMovie.Padding = new System.Windows.Forms.Padding(3);
            this.tpMovie.Size = new System.Drawing.Size(419, 321);
            this.tpMovie.TabIndex = 1;
            this.tpMovie.Text = "Movies";
            this.tpMovie.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Director";
            // 
            // cbxDirector
            // 
            this.cbxDirector.FormattingEnabled = true;
            this.cbxDirector.Location = new System.Drawing.Point(17, 56);
            this.cbxDirector.Name = "cbxDirector";
            this.cbxDirector.Size = new System.Drawing.Size(121, 21);
            this.cbxDirector.TabIndex = 0;
            this.cbxDirector.SelectedIndexChanged += new System.EventHandler(this.cbxDirector_SelectedIndexChanged);
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miEdit});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(518, 24);
            this.msMain.TabIndex = 1;
            this.msMain.Text = "menuStrip";
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miExit});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(37, 20);
            this.miFile.Text = "&File";
            // 
            // miSave
            // 
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(98, 22);
            this.miSave.Text = "&Save";
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(98, 22);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // miEdit
            // 
            this.miEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.miCopy,
            this.miTopping});
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(39, 20);
            this.miEdit.Text = "E&dit";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(181, 6);
            // 
            // miCopy
            // 
            this.miCopy.Name = "miCopy";
            this.miCopy.Size = new System.Drawing.Size(184, 22);
            this.miCopy.Text = "C&opy";
            this.miCopy.Click += new System.EventHandler(this.miCopy_Click);
            // 
            // miTopping
            // 
            this.miTopping.Name = "miTopping";
            this.miTopping.Size = new System.Drawing.Size(184, 22);
            this.miTopping.Text = "Topping Master Data";
            this.miTopping.Click += new System.EventHandler(this.miTopping_Click);
            // 
            // ssMain
            // 
            this.ssMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statLblMain,
            this.progBarMain});
            this.ssMain.Location = new System.Drawing.Point(0, 399);
            this.ssMain.Name = "ssMain";
            this.ssMain.Size = new System.Drawing.Size(518, 22);
            this.ssMain.TabIndex = 2;
            this.ssMain.Text = "ssMain";
            // 
            // statLblMain
            // 
            this.statLblMain.AutoSize = false;
            this.statLblMain.Name = "statLblMain";
            this.statLblMain.Size = new System.Drawing.Size(100, 17);
            // 
            // progBarMain
            // 
            this.progBarMain.Maximum = 200000;
            this.progBarMain.Name = "progBarMain";
            this.progBarMain.Size = new System.Drawing.Size(100, 16);
            // 
            // clbMovies
            // 
            this.clbMovies.FormattingEnabled = true;
            this.clbMovies.Location = new System.Drawing.Point(17, 92);
            this.clbMovies.Name = "clbMovies";
            this.clbMovies.Size = new System.Drawing.Size(205, 94);
            this.clbMovies.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.clbMovies);
            this.panel1.Controls.Add(this.cbxDirector);
            this.panel1.Location = new System.Drawing.Point(6, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(336, 229);
            this.panel1.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Select Favorite Movies";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 421);
            this.Controls.Add(this.ssMain);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.msMain);
            this.MainMenuStrip = this.msMain;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Daves Awesome Application";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.tabMain.ResumeLayout(false);
            this.tpPizza.ResumeLayout(false);
            this.tpPizza.PerformLayout();
            this.tpMovie.ResumeLayout(false);
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.ssMain.ResumeLayout(false);
            this.ssMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tpPizza;
        private System.Windows.Forms.TabPage tpMovie;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.StatusStrip ssMain;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ToolStripMenuItem miEdit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miCopy;
        private System.Windows.Forms.ToolStripStatusLabel statLblMain;
        private System.Windows.Forms.ToolStripProgressBar progBarMain;
        private System.Windows.Forms.DateTimePicker dtpBirthday;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbNo;
        private System.Windows.Forms.RadioButton rbYes;
        private System.Windows.Forms.CheckBox cbLikePizza;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem miTopping;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxDirector;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolStripMenuItem miSave;
        private System.Windows.Forms.CheckedListBox clbMovies;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
    }
}

