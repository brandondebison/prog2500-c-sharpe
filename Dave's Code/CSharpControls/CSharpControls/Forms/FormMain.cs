﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Specialized;

namespace CSharpControls.Forms
{
    public partial class FormMain : Form
    {
        const string c_FrmToppings = "TOPPINGS";
        public NameValueCollection nvcDirMovies = new NameValueCollection();
        public string[] arrDirectors = {"Cameron James","Steven Spielberg","Quentin Tarantino","Stanley Kubrick"};

        public FormMain()
        {
            InitializeComponent();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void miCopy_Click(object sender, EventArgs e)
        {
            string CurItem = "";
            if (tabMain.SelectedIndex == 0)
            {
                CurItem = "Pizza";
            
            }
            else
            {
                CurItem = "Movie";
            }

            if  (MessageBox.Show(string.Format("Would you Like to Copy the {0} data?",CurItem),"Copy?",MessageBoxButtons.YesNo ) == DialogResult.Yes)
            {
                statLblMain.Text = "Copying...";
                this.Refresh();
                for (int i = 1; i <= 200000; i++)
                {
                    progBarMain.Increment(1);
                }
                statLblMain.Text = "Copy Complete";
            }
        }

        private void cbLikePizza_CheckedChanged(object sender, EventArgs e)
        {
            setRbEnabled(cbLikePizza.Checked);
        }

        private void setRbEnabled(bool enabled)
        {
            rbNo.Enabled = enabled;
            rbYes.Enabled = enabled;

        }

        private void miTopping_Click(object sender, EventArgs e)
        {
            showForm(c_FrmToppings);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {   
            populateDirectors(cbxDirector);
            initMovies();
        }
        
        private void showForm(string formName)
        {
            switch (formName)
            {
                case c_FrmToppings:
                    FormToppings frmTop = new FormToppings();
                    frmTop.Show();
                    break;
            }

        }

        private void populateDirectors(ComboBox directorsCombo)
        {
            directorsCombo.Sorted = true;
            foreach (string director in arrDirectors)
            {
                directorsCombo.Items.Add(director);
            }
            
        }

        private void initMovies()
        {
            for (int i = 0; i <= arrDirectors.Length - 1; i++)
            {
                switch (i)
                {

                    case 0:
                        nvcDirMovies.Add(arrDirectors[i], "Avatar");
                        nvcDirMovies.Add(arrDirectors[i], "Titanic");
                        nvcDirMovies.Add(arrDirectors[i], "Terminator");
                        nvcDirMovies.Add(arrDirectors[i], "The Abyss");
                        break;
                    case 1:
                        nvcDirMovies.Add(arrDirectors[i], "E.T.");
                        nvcDirMovies.Add(arrDirectors[i], "Jaws");
                        nvcDirMovies.Add(arrDirectors[i], "Raiders of the Lost Ark");
                        nvcDirMovies.Add(arrDirectors[i], "Back to the future");
                        break;
                    case 2:
                        nvcDirMovies.Add(arrDirectors[i], "Pulp Fiction");
                        nvcDirMovies.Add(arrDirectors[i], "Sin City");
                        nvcDirMovies.Add(arrDirectors[i], "From Dusk Till Dawn");
                        break;
                    case 3:
                        nvcDirMovies.Add(arrDirectors[i], "The Shining");
                        nvcDirMovies.Add(arrDirectors[i], "A Clockwork Orange");
                        nvcDirMovies.Add(arrDirectors[i], "2001: A space Oddity");
                        break;
                }

            }

        }

        private void populateMovies(string Director)
        {
            clbMovies.Items.Clear();
            string[] movs = nvcDirMovies.GetValues(Director);
            foreach (string Movie in movs)
            {
                clbMovies.Items.Add(Movie);
            }
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dtpBirthday.Value >= DateTime.Now.AddYears(-18))
            {
                MessageBox.Show("Must be 18 years or older to be a customer");
            }
        }

        private void cbxDirector_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateMovies(cbxDirector.Text);
        }
    }
    }
