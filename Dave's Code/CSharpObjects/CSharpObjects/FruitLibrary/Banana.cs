﻿using System;
using System.Drawing;

namespace FruitLibrary
{
    class Banana : Fruit
    {
       
        private int skin = 25;

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Banana() : base()
        {
            this.Color = Color.Yellow;
            
        }






        /// <summary>
        /// Constructor of the Banana Class
        /// </summary>
        /// <param name="Color">Color of the Banana, only allows Yellow, Green or Brown</param>
        /// <param name="Age">Age of the Banana in Days</param>
        /// <param name="Weight">Weight of the Banana in Grams</param>
        public Banana(Color Color, int Age, float Weight)
        {
            this.Color = Color;
            this.Age = Age;
            this.Weight = Weight;
        }


     

        /// <summary>
        /// This Porperty Will return the color of the banana
        /// </summary>
        public override Color Color
        {
            get
            {
                return color;
            }
            set
            {
                if (value == Color.Yellow || value == Color.Green || value == Color.Brown)
                {
                    color = value;
                }
                else
                {
                    throw new Exception(String.Format("{0} is an Invalid Banana Color", value));
                }

            }
        }

        /// <summary>
        /// Read Only Propery that returns the Name
        /// </summary>
        public override string Description
        {
            get
            {
                return "Banana";
            }
        }

        /// <summary>
        /// Implementation of the Peel method, private encapsulation to hide implementation details
        /// (Reduce the Skin private member until it reaches 0), Once it reaches 0, the IsPeeled Flag is set
        /// to true
        /// </summary>
        /// 
        private void PeelBanana()
        {
            while (this.skin > 0)
            {
                this.skin -= 5;
            }
            IsPeeled = true;
        }



        /// <summary>
        /// This Method will Peel the Banana
        /// </summary>
        public override void Peel()
        {
            this.PeelBanana();
        }

        /// <summary>
        /// Returns the Ripen time of the Banana in Days
        /// </summary>
        /// <returns>Ripen Time in Days</returns>
        public override int getRipenTime()
        {
            return 5;
        }
    }
}
