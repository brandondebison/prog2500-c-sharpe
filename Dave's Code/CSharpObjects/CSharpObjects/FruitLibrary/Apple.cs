﻿// Import Namespaces.
using System;
using System.Drawing;

namespace FruitLibrary
{
    sealed class Apple : Fruit
    {
        private int skin = 100;

        /// <summary>
        /// Default Constructor.
        /// </summary>
       public Apple() : base()
        {
            this.color = Color.Red;
        }
    
        /// <summary>
        /// Constructor of the Apple Class
        /// </summary>
        /// <param name="Color">Color of the Apple, only allows red, Green or Brown</param>
        /// <param name="Age">Age of the Apple in Days</param>
        /// <param name="Weight">Weight of the apple in Grams</param>
        public Apple(Color Color, int Age, float Weight)
        {
            this.Color = Color;
            this.Age = Age;
            this.Weight = Weight;
        }

        /// <summary>
        /// This Porperty Will return the color of the apple
        /// </summary>
        public override Color Color
        {
            get
            {
                return color;
            }
            set
            {
                if (value == Color.Red || value == Color.Green || value == Color.Brown)
                {
                    color = value;
                }
                else
                {
                    throw new Exception(String.Format("{0} is an Invalid Apple Color",value));
                }

            }
        }

        /// <summary>
        /// Read Only Propery that returns the Name
        /// </summary>
        public override string Description
        {
            get
            {
                return "Apple";
            }
        }
        


        /// <summary>
        /// Implementation of the Peel method, private encapsulation to hide implementation details
        /// (Reduce the Skin private member until it reaches 0), Once it reaches 0, the IsPeeled Flag is set
        /// to true
        /// </summary>
        private void PeelApple()
        {
            while (this.skin > 0)
            {
                this.skin -= 10;
            }
            IsPeeled = true;
        }

        /// <summary>
        /// This Method will Peel the Apple 
        /// </summary>
        public override void Peel()
        {
            this.PeelApple();
        }

        /// <summary>
        /// Returns the Ripen time of the Apple in Days
        /// </summary>
        /// <returns>Ripen Time in Days</returns>
        public override int getRipenTime()
        {
            return 20;
        }
        
    }
}
