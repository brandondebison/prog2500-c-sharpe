﻿using System.Drawing;

namespace FruitLibrary
{
    abstract class Fruit
    {
        //Fields
        protected Color color;
       
        /// <summary>
        /// Base Constructor
        /// </summary>
        public Fruit()
        {
            this.Age = 0;
            this.Weight = 0.0f;
        }

        //Auto implemented Properties
        public float Weight { get; set; }
        public int Age { get; set; }
        public bool IsPeeled { get; set; }


        //Abstract Property
        public abstract Color Color{ get; set; }
        public abstract string Description { get; } // Read Only Property

        // Abstract Methods - Must be implemented by Inheriting Classes
        public abstract int getRipenTime();
        public abstract void Peel();
    }
}
