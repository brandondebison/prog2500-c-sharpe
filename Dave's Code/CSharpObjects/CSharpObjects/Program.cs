﻿using System;
using System.Collections.Generic;
using FruitLibrary;
using System.Drawing;

namespace CSharpObjects
{
    class Program
    {
        static void Main(string[] args)
        {
         
            // This is the fruit stand shelf. This method will place Apples
            // and Bananas on the shelf.
            List<Fruit> shelf = StockShelf();
            

            // Select Fruit For purchase from the shelf and put them in them in the basket
            List<Object> basket = SelectFruit(shelf);

            // A banana out of the basket it, peel it and eat it.
            Banana ban1 = GetBanana(basket);
            if (ban1 != null)
            {
                ban1.Peel();
                if (ban1.IsPeeled)
                {
                    EatBanana(ban1);
                }
            }

        }

        static List<Fruit> StockShelf()
        {
            List<Fruit> shelf = new List<Fruit>();

            // Used to generate random Weights and Ages
            Random rand = new Random();

            //At 10 of each fruit type to the shelf
            for (int i = 0; i < 10; i++)
            {
                Apple apple = new Apple(Color.Red, rand.Next(1, 20), rand.Next(70, 100));
                shelf.Add(apple);
                Banana banana = new Banana();
                banana.Color = Color.Yellow;
                banana.Age = rand.Next(1, 8);
                banana.Weight = rand.Next(100, 130);
                shelf.Add(banana);
            }
            return shelf;
        }

        static List<Object> SelectFruit(List<Fruit> shelf)
        {
            int appleCount = 0;
            int bananaCount = 0;

            // Baskets can contain different kinds of objects.
            // Select fruit for purchase based on our criteria 
            // Object is the base class of all objects (all objects created inherit from object)
            // Therefore we are able to store objects of different types in references of type object
            List<Object> basket = new List<Object>();

            foreach (Fruit fruit in shelf)
            {
                if (fruit is Apple)
                {
                    if (appleCount < 3)
                    {
                        if (fruit.Age <= 10 & fruit.Weight > 80)
                        {
                            basket.Add(fruit);
                            appleCount++;
                        }
                    }
                }
                else if (fruit is Banana)
                {
                    if (bananaCount <= 6)
                    {
                        if (fruit.Age <= 3 & fruit.Weight < 110)
                        {
                            basket.Add(fruit);
                            bananaCount++;
                        }
                    }
                }
                
            }
            return basket;
        }

        static Banana GetBanana(List<Object> basket)
        {
 
            foreach(Fruit ban in basket)
            {
                if (ban is Banana)
                {
                    // Return Must be type casted to a Banana Object
                    return (Banana)ban;
                }
            }
            return null;
        }

        static void EatBanana(Banana banana)
        {
            //To Do , Create Person Class and IMplement Eat Method
        }
    }

    //class Apple1 : Apple
    //{

    //}

    //class Banana1 : Banana
    //{

    //}
}
